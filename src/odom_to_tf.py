#!/usr/bin/env python
import rospy
import tf
from nav_msgs.msg import Odometry

def odometry_callback(msg):
    position = msg.pose.pose.position
    orientation = msg.pose.pose.orientation
    print(output_frame_id, output_child_frame_id)
    broadcaster.sendTransform((position.x, position.y, position.z),
                              (orientation.x, orientation.y, orientation.z, orientation.w),
                              msg.header.stamp,
                              output_child_frame_id,
                              output_frame_id)

if __name__ == "__main__":
    rospy.init_node("odometry_to_tf_node")
    
    output_frame_id = rospy.get_param('~output_frame_id', 'map')
    output_child_frame_id = rospy.get_param('~output_child_frame_id', 'uav')
    
    broadcaster = tf.TransformBroadcaster()
    
    rospy.Subscriber("odom", Odometry, odometry_callback)

    rospy.spin()
